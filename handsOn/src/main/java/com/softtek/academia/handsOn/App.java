package com.softtek.academia.handsOn;

import java.sql.Connection;

import com.softtek.academia.handsOn.Device;
import com.softtek.academia.handsOn.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {

		List<Device> result = new ArrayList<>();
		String SQL_SELECT = "Select * from device";
		Connection conn = null;

		try {
			conn = DBConnection.connect();
			if (conn != null) {
				PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT);
				ResultSet resultSet = preparedStatement.executeQuery();
				while (resultSet.next()) {
					Device device = new Device(resultSet.getLong("DEVICEID"), resultSet.getString("NAME"),
							resultSet.getString("DESCRIPTION"), resultSet.getLong("MANUFACTURERID"),
							resultSet.getLong("COLORID"), resultSet.getString("COMMENTS"));
					result.add(device);
				}
				//result.forEach(System.out::println);
				List<String> deviceNames = result.stream()
						.map(Device::getName)
						.filter(device -> device.toUpperCase().contains("LAPTOP"))
						.collect(
								Collectors.toList()
						);
				System.out.println(deviceNames.toString());
				
				long manufacturer3 = result.stream()
						.map(Device::getManufacturerId)
						.filter(id -> id ==3)
						.count();
				System.out.println("Number of Devices with manufacturerId equals to 3: " + manufacturer3);
				
				List<Device> devices = result.stream()
						.filter(d -> d.getColorId() == 1L)
						.collect(
								Collectors.toList()
						);
				//devices.forEach(System.out::println);
				Map<Long, Device> deviceMap= result.stream()
                        .collect(
                                Collectors.toMap(Device::getDeviceId,Device::getDevice)
                        );
				System.out.println(deviceMap.toString());
			}
		} catch (SQLException e) {
			System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
