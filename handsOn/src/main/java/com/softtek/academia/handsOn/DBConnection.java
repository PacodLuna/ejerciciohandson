package com.softtek.academia.handsOn;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
	private static Connection conn = null;

	public DBConnection() {}

	public static Connection connect() {
		if (conn == null) {
			try {
				conn = DriverManager.getConnection(
						"jdbc:mysql://127.0.0.1:3306/sesion3?serverTimezone=UTC#", "root","1234");
				System.out.println("Connected to the database!");
			} catch (SQLException e) {
	            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
	        } catch (Exception e) {
	            e.printStackTrace();
	        }  
		}
		return conn;
	}
}